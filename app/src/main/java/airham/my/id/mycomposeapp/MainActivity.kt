package airham.my.id.mycomposeapp

import airham.my.id.mycomposeapp.ui.theme.MyComposeAppTheme
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyComposeAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = Color.Yellow) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Greeting("Android")
                        Divider(thickness = 5.dp, color = Color.DarkGray)
                        Greeting("all")
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!", color = Color.DarkGray)
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MyComposeAppTheme {
        Greeting("Android")
    }
}

@Preview("card preview")
@Composable
fun testPreview() {
    Row(
        modifier = Modifier.fillMaxWidth(fraction = 0.8F),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        Greeting("Tes")
        Greeting("Coba")
    }
}

@Preview
@Composable
fun CardPreview(){
    Card(){
        Row(){

        }
    }
}

@Preview
@Composable
fun makeText(){
    val name = remember {

    }
}